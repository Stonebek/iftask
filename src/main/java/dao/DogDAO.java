package dao;

import model.Dog;

public class DogDAO extends GenericDAO<Dog>{

	public DogDAO() {
		super(Dog.class);
		// TODO Auto-generated constructor stub
	}
	private static final long serialVersionUID = 1L;
	public void delete(Dog dog) {
        super.delete(dog.getId(), Dog.class);
    }
}
