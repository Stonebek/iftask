package facade;

import java.util.List;

import dao.DogDAO;
import model.Dog;

public class DogFacade {
	private DogDAO dogDAO = new DogDAO();

	public void createDog(Dog dog) {
		dogDAO.beginTransaction();
		dogDAO.save(dog);
		dogDAO.commitAndCloseTransaction();
	}

	public void updateDog(Dog dog) {
		dogDAO.beginTransaction();
		Dog persistedDog = dogDAO.find(dog.getId());
		persistedDog.setAge(dog.getAge());
		persistedDog.setName(dog.getName());
		dogDAO.update(persistedDog);
		dogDAO.commitAndCloseTransaction();
	}

	public Dog findDog(int dogId) {
		dogDAO.beginTransaction();
		Dog dog = dogDAO.find(dogId);
		dogDAO.closeTransaction();
		return dog;
	}

	public List<Dog> listAll() {
		dogDAO.beginTransaction();
		List<Dog> dogList = dogDAO.findAll();
		dogDAO.closeTransaction();
		return dogList;
	}

	public void deleteDog(Dog dog) {
		dogDAO.beginTransaction();
		Dog persistedDog = dogDAO.findReferenceOnly(dog.getId());
		dogDAO.delete(persistedDog);
		dogDAO.commitAndCloseTransaction();
	}
}
